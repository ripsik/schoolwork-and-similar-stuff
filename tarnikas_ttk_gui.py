#Pere sissetuleku kalkulaator.

#Huhh, tkinter ja ttk tunduvad kuidagi väga puised ja kohmakad. Küllap ma kasutan neid ka väga valesti

from tkinter import *
from tkinter import ttk



def arvuta(*args):
    #mis argumente ta saab? Ei midagi kasulikku. Või kui see on entry nimi, siis saaks ta ka punaseks värvida, kui sisu ei meeldi.
    #TODO: ilus valideerimine

    tulumaks = 0.21 #protsendina
    maksuvaba = 144 #maksuvaba miinimum
    lapsetoetus = 20


    try:

        try: #suvasisestuse korral ei taha crashi. Tahan, et kastike oleks punase tekstiga,(või tühi), ja arvestataks nullina.
            ema_palk = float(EmaBruto.get())
        except:
            ema_palk = 0

        try:
            isa_palk = float(IsaBruto.get())
        except:
            isa_palk = 0

        try:
            lastearv = float(LasteArv.get())
        except:
            lastearv = 0

        #hakkab arvutama
        ema_maksustatav = max(ema_palk - maksuvaba, 0)
        ema_neto = ema_palk - ema_maksustatav * tulumaks

        isa_maksustatav = max(isa_palk - maksuvaba, 0)
        isa_neto = isa_palk - isa_maksustatav * tulumaks

        laste_neto = lastearv * lapsetoetus

        #annab info edasi
        Sissetulek.set( str( round(ema_neto + isa_neto + laste_neto, 2) ) )
        EmaNeto.set( str( round(ema_neto, 2) ) )
        IsaNeto.set( str( round(isa_neto, 2) ) )
        Lastetoetus.set( str( laste_neto ) )


        update_graph(ema_neto, isa_neto, laste_neto)

    except:
        #print("Errors are still happening?")
        #pass
        raise



def update_graph(ema, isa, laps):
    graph.delete(ALL)

    #extents
    ext_ema = ema / (ema+isa+laps) * 360
    ext_isa = isa / (ema+isa+laps) * 360
    ext_laps = laps / (ema+isa+laps) * 360

    #create_arc ilmselt teeb omakorda 360 - ext tehte. Ehk kui anda talle täpselt 360, siis ta arci ei joonista?! Grr.
    if ext_ema == 360: ext_ema -= 0.00001
    if ext_isa == 360: ext_isa -= 0.00001
    if ext_laps == 360: ext_laps -= 0.00001

    #joonista keskele. Idee oli teha koos aknaga suurust muutvaks.
    w = graph.winfo_width()
    h = graph.winfo_height()

    graph.create_arc( 10,10,w-10,h-10,  start=0, extent = ext_ema, fill=ema_värv)
    graph.create_arc( 10,10,w-10,h-10,  start=ext_ema, extent = ext_isa, fill=isa_värv)
    graph.create_arc( 10,10,w-10,h-10,  start=ext_ema + ext_isa, extent = ext_laps, fill=laps_värv)

    graph.create_rectangle(1,1, w-2, h-2) #äärmine piksliveerg ei ole nähtav?







######################################################################################################################
#GUI koostamine:
root = Tk()
root.title("Pere sissetulek")

#update_graph võtab ka värvid siit;)
ema_värv = 'maroon'
isa_värv = 'dark green'
laps_värv = 'purple'


#TTK stiilid on üksjagu peavalu valmistavad..
s = ttk.Style()
s.theme_use('classic')

s.configure('sissetulek.TLabel', font='bold', padding="0 20 0 0")
s.configure('ema.TLabel', foreground=ema_värv)
s.configure('isa.TLabel', foreground=isa_värv)
s.configure('laps.TLabel', foreground=laps_värv)
#print(s.lookup('laps.TLabel', 'foreground'))  #Hmm, seda saaks äkki update_graphis kasutada
#TODO: oleks nagu näinud, et sticky käib ka stiili sisse?


#Tühja kah, hakkab ehitama.
frame = ttk.Frame(root, padding="3 3 3 3")
frame.grid(column=0, row=0, sticky=(N, W, E, S))
frame.columnconfigure(0, weight=1)
frame.rowconfigure(0, weight=1)


#Suhtlus GUI ja programmiloogika vahel käib läbi seotud muutujate:
#TODO: Erinevat tõstustiili kasutan paremaks äratundmiseks. Vahest oleks mingi prefiks parem? var_?
EmaBruto = StringVar()
IsaBruto = StringVar()
LasteArv = StringVar()

#väljund
Sissetulek  = StringVar()
EmaNeto     = StringVar()
IsaNeto     = StringVar()
Lastetoetus = StringVar()



#Sisestuskastid
ema_palk_box = ttk.Entry(frame, width=7, textvariable=EmaBruto)
isa_palk_box = ttk.Entry(frame, width=7, textvariable=IsaBruto)
laste_arv_box = ttk.Entry(frame, width=7, textvariable=LasteArv)


#Sisestuskastide paigutus
ema_palk_box.grid(column=1, row=0, sticky=(W, E))
isa_palk_box.grid(column=1, row=1, sticky=(W, E))
laste_arv_box.grid(column=1, row=2, sticky=(W, E))



#Selgitavad Labelid
ttk.Label(frame, text="Ema bruto (€):").grid(column=0, row=0, sticky=(W, E))
ttk.Label(frame, text="Isa bruto (€):").grid(column=0, row=1, sticky=(W, E))
ttk.Label(frame, text="Lapsi (tk):").grid(column=0, row=2, sticky=(W, E))

ttk.Label(frame, text="Sissetulek (€):", style='sissetulek.TLabel').grid(column=0, row=4, sticky=(W, E))
ttk.Label(frame, text="*Ema neto (€):", style='ema.TLabel').grid(column=0, row=5, sticky=(W, E))
ttk.Label(frame, text="*Isa neto (€):", style='isa.TLabel').grid(column=0, row=6, sticky=(W, E))
ttk.Label(frame, text="*Lastetoetus (€):", style='laps.TLabel').grid(column=0, row=7, sticky=(W, E))

# Seda nuppu poleks tegelikult vaja - proovima, kas saame ilma.
#ttk.Button(frame, text="Arvuta", command=arvuta).grid(column=0, row=4, sticky=(W,E), columnspan=2)


#Väljund
#netopalgalabelid
ttk.Label(frame, textvariable=Sissetulek, style='sissetulek.TLabel').grid(column=1, row=4, sticky=(W, E))
ttk.Label(frame, textvariable=EmaNeto, style='ema.TLabel').grid(column=1, row=5, sticky=(W, E))
ttk.Label(frame, textvariable=IsaNeto, style='isa.TLabel').grid(column=1, row=6, sticky=(W, E))
ttk.Label(frame, textvariable=Lastetoetus, style='laps.TLabel').grid(column=1, row=7, sticky=(W, E))

#piechart läheb oma canvase peale
graph = Canvas(height=150, width=150)
graph.grid(column=0, columnspan=2, row=8, sticky=(W, E))


#automaatne arvutamine
EmaBruto.trace_variable("w", arvuta)
IsaBruto.trace_variable("w", arvuta)
LasteArv.trace_variable("w", arvuta)


#tekitab veidi ruumi juurde
for child in frame.winfo_children():
    child.grid_configure(padx=5, pady=5)


ema_palk_box.focus()

root.mainloop()
