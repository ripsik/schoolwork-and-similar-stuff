
'''
Idee: Maailmas ei ole olemas lõputut hulka sõnu - seega index sõnadest võiks märksa ruumisäästlikum ja kiirem olla
FILES:
    fileid, fullpath, some hashorsmthng
WORDS:
    wordid, word
WORDSINFILES:
    wordid, fileid 

1) otsingu algul vt, kas on loetelu adekvaatne - st kõik kaustas leiduvad failid andmebaasis.
2) kui ei ole, siis scan files. Delete baasist need, mida ei leia.
3) kui on, siis tuleb indeksist otse välja loetelu failidest - mida siis omakorda avada ja sobilik koht ette näidata.

'''
import re
import os
import hashlib
import sqlite3

def getTextFromPDF( filename ):
    ret = ''
    if os.system('pdftotext "'+filename+'" tempcontents.txt') == 0: #st et konverteerimine õnnestus
        with open('tempcontents.txt', 'r') as f: #TODO: utf8 ei ole tarvilik vmt?
            ret = f.read()
        #TODO: os.remove() ajutise faili kustutamiseks
    else:
        print( 'Failed for whatever reason: ' + filename)
    return ret

'''
    quick and dirty hash from filename, size, creation date, and perhaps some, but not all, contents?
'''
def getHash( filename ): # i expect full path!? Actually, not really. Folder can be located somewhere else... parentfolder should be same.
    size = os.path.getsize( filename )
    with open( filename, 'rb') as f:
        content = f.read(2000)
    return hashlib.sha1(content + bytes(size)).hexdigest() #TypeError: can't concat int to bytes
#print (getHash('prooviks.pdf'))

'''
    Võta fail, ja vaata, kas juba eksisteerib tabelis. Kui jah, siis goodbye. Kui ei, siis käivitab insertFile.
'''
def lookupFile( filename ):
    hash = getHash( filename )
    
    cur.execute('SELECT EXISTS(SELECT 1 FROM FILES WHERE path = ? AND hash = ?);', (filename, hash) )
    if cur.fetchone()[0]:
        print( 'Exists, ignored: ' + filename )
    else:
        insertFile( filename, hash )



'''
    Ei olnud seda faili juba baasis. Selge, parsime läbi ja paneme kuhu vaja, mida vaja.
'''
def insertFile( filename, hash):
    print( 'Going to insert: ' + filename )
    # 1) file's metadata
    cur.execute('insert or ignore into FILES values (null,?,?)', (filename, hash))
    cur.execute('select id from FILES where path = ? AND hash = ?', (filename, hash))
    fid = cur.fetchone()[0]
    
    # 2) get and add additional words and connections 
    txt = getTextFromPDF( filename ).lower()
    words = re.findall(r'\w+', txt)
    uniquewords = set( words )
    print ( uniquewords )
    
    #igasugust pahna ei taha... nimeliselt numbreid ja 1-tähelisi
    for w in uniquewords:
        if not w.isdigit() and len(w) >= 3:
            cur.execute('insert or ignore into WORDS values (null, ?)', [w])
            #nüüd kindlasti on, saan id...
            cur.execute('select id from WORDS where word = ?', [w])
            wid = cur.fetchone()[0]
            cur.execute('insert or ignore into WORDSINFILES values (?,?)', [fid, wid])
    
    db.commit()
    pass


'''
    aga võib ka juhtuda, et vahepeal on n faili asukohta muutnud, ära kustutatud vmt... Siis võiks kõik deprecated kirjed baasist välja visata.
'''
def cleanDB():
    pass

'''
    kuidas kasutada?
    
'''



######################################################################
######################################################################
db = sqlite3.connect('index.db')

db.execute('''create table if not exists FILES
                (ID integer primary key, 
                 PATH           text,
                 HASH           text,
                 CONSTRAINT uniq_file UNIQUE (PATH, HASH)
                );
           ''')
db.execute('''create table if not exists WORDS
                (ID integer primary key,
                 WORD          text,
                 CONSTRAINT uniq_word UNIQUE (WORD)
                );
           ''')
db.execute('''create table if not exists WORDSINFILES
                (FILEID int,
                 WORDID int,
                 CONSTRAINT uniq_connect UNIQUE (FILEID, WORDID)
                );
           ''')

cur = db.cursor()


cur.execute('select * from files')
print( cur.fetchall() )

cur.execute('select * from words')
print( cur.fetchall() )


'''
cur.executemany("INSERT INTO Cars VALUES(?, ?, ?)", cars)

insert NULL instead of autoincrement

lid = cur.lastrowid


'''

lookupFile( 'prooviks.pdf')

db.close()