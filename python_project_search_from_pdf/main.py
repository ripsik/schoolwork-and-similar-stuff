#TODO: stand-alone exe saamiseks vt: http://www.pyinstaller.org/
import os, sys, subprocess #failidega jagelemiseks
import configparser #viimatikasutatud kaust ei taha meelde jääda? Head lahendust ei ole, aga Configparser näikse olevat kõige enam kasutatav. 
from appJar import gui
'''
http://appJar.info
"The easiest way to create GUIs in Python."
Kasutab "kapoti all" TKinterit, aga pakub lihtsamat pöördumisviisi.
'''

#import PyPDF2 #Ei tööta?!? Ainult müra annab. https://automatetheboringstuff.com/chapter13/
#import pdftotext #pip install pfftotext, aga selleks oli vaja enne ka poppler'it, mis on xpdfi edasiarendus
#https://askubuntu.com/questions/781552/how-to-install-the-latest-version-of-poppler
#windows 10 all: thonny Tools->Manage packages, otsi pakki:
#pdftotext, install
#failed,  error: Microsoft Visual C++ 14.0 is required.
#Got some from: http://landinghub.visualstudio.com/visual-cpp-build-tools
#Windows10 SDK-ga koos 6GB!?!?
#Ikka error. Python.h is missing. Viskab käega, liiga palju installimist.

'''
Variant oleks kasutada ka vastavat käsurea-utiliiti... 
Aadressilt http://www.xpdfreader.com/dl/xpdf-tools-win-4.00.zip
Zipi seest bin64/pdftotext.exe skripti kausta (või PATHi)
Loogika oleks siis: tekita pdfist ajutine tekstifail, töötle, võta järgmine.
'''
def getTextFromPDF( filename ):
   
    ret = ''
    if os.system('pdftotext "'+filename+'" tempcontents.txt') == 0: #st et konverteerimine õnnestus, veakoodi ei anta
        with open('tempcontents.txt', 'r') as f: #utf8 ei ole tarvilik? 'r' - st loeb failisisu (teksti)
            ret = f.read()
        #TODO: os.remove() ajutise faili kustutamiseks
    else:
        print( 'Failed for whatever reason: ' + filename)
    return ret
#print(getTextFromPDF( "prooviks2.pdf"))


# Võtab ühe pdf faili, ja tagastab esialgu vaid True/False - et kas on või ei ole.
# otsing on tõstu- ja tühikutundetu. Ainult (ladina(ja eesti))tähed-numbrid... Sõnadtehakseüheksjoruks, mis võib põhjustada soovimatuid vasteid...
def pdfContainsWord( keyword, filename ):
    content = getTextFromPDF(filename)
    #print(content) - väga palju saasta...
    #järgnevalt üleliigse kraami eemaldamine, jätab järele ainult väikesed tähed
    modif = ''
    keymodif = ''
    for c in content.lower():
        if c in 'abcdefghijklmnopqrstuvxyzõäöü1234567890@':
            modif += c
    for c in keyword.lower():
        if c in 'abcdefghijklmnopqrstuvxyzõäöü1234567890@':
            keymodif += c
    return keymodif in modif 
#print( pdfContainsWord( 'randoja', 'prooviks2.pdf' ) )


#võtab parasjagu valitud kaustanime jmt, ning säilitab need järgmiseks korraks
def updateConf():
    conf['last']['directory'] = app.getEntry('ChooseDir')
    conf['last']['keyword']   = app.getEntry('KeywordEntry')
    
    with open('config.ini', 'w', encoding='utf-8') as f: 
        conf.write(f)    


# Kui vajutatakse Search nuppu (või ENTERit otsisõna sisestamisel), siis käivitub otsing
# Kuvab ka progress-bar'i
def searchCallback(*argv):
    
    updateConf()
    dir  = conf['last']['directory']
    word = conf['last']['keyword']
    
    listofpdfs = []
    interestingpdfs = []
    app.updateListBox('listOfFiles', [], select=False) #tühjendab nimekirja 

    #otsib kõik pdfid üles
    for file in os.listdir(dir): 
        if file.endswith(".pdf"):
            #print( file )
            listofpdfs.append( file )
            
    #nüüd ma tean, mitu pdfi kaustas on, ja saan progress/bari näidata...        
    app.showMeter('progress')
    i = 1
    for file in listofpdfs:
         app.setMeter('progress', i / len(listofpdfs) * 100, str(i) + '/' + str( len(listofpdfs) ) ) #protsent ja tekst 
         i += 1
         if pdfContainsWord( word, dir + '\\' + file):
             interestingpdfs.append( file )
    app.hideMeter('progress')
    
    interestingpdfs.append('') #tühirida, et suvaline hiireklõps ei käivitaks viimast faili
    app.updateListBox('listOfFiles', interestingpdfs, select=False) #kuidagi kaua võtab uuendamine, või selle kuvamine, aega?



# Hiireklõps failide loetelul avab kursori alla jääva faili.
# Topeltklõpsu ei saanud tööle. ListBox ei ole vahest kõige õigem viis esitamiseks.
# TODO: oleks vaja midagi, kus oleks ka failipath ja väljavõte sisust...
def listCallback(*argv):
    #print('listCallback reports: ' + str( app.getListBox('listOfFiles') ) ) #valitud väärtus?
    #Nähtavasti getListBox annab valitud read listina. Aga et ma soovin ainult üht faili avada, siis...
    selected = app.getListBox('listOfFiles')
    dir = app.getEntry('ChooseDir')

    if len(selected) > 0 and len(selected[0]) > 0: #st et ListBox annab stringide järjendi ja järjendis on midagi ning see pole tühi rida, siis käivitab
        os.startfile( dir + '\\' + selected[0] )



#####################################################################################################
#eelmise korra väärtused konfiguratsioonifailist
#https://docs.python.org/3/library/configparser.html
conf = configparser.ConfigParser()
conf.read('config.ini', encoding='utf-8') 


# 1) tekitab graafilise kasutajaliidese akna...
app = gui('Search a keyword from pdf files', '400x500')
app.setSticky('ew')
app.setStretch('column')
app.setPadding([5,5])

# 2) ...millesse lisab Widgeteid. Esmalt standardne kaustavalimise koht.
app.addDirectoryEntry('ChooseDir')
app.setEntry('ChooseDir', conf['last']['directory'] ) #eelmine kasutatud kaust


# 3) tekitab tekstikastikese, kuhu sisestada otsitav sõna
# TODO: Kas AutoEntry pakub ka mälu, st varemsisestatud väärtuseid?
app.addEntry('KeywordEntry')
#app.addAutoEntry('KeywordEntry', ['yksi', 'kaksi', 'kolmi', 'nelja']) #autoEntry on mõistlikum siis, kui eelnevalt on analüüsitud ja indekseeritud kõik pdfid ühte andmebaasi - siis pakubki lihtsalt kõiki esinevaid sõnu...
#app.setEntryDefault('KeywordEntry', "Enter word to be searched" )
app.setEntry('KeywordEntry', conf['last']['keyword'] ) #kui tahta, et viimatine otsisõna ees oleks

# 4) Nupp, mis käivitab otsingu. Ja paneme otsisõna-lahtri reageerima ENTERile.
app.addButton('Search', searchCallback)
app.setEntrySubmitFunction('KeywordEntry', searchCallback)

# 5) pdfide lugemine ei ole kiire, proovime progress-bari näidata. Aga ainult siis, kui vaja on.
app.addMeter('progress')
app.hideMeter('progress')

# 6) leitud failide kuvamine. Seda kasti tahan nii suurt kui võimalik.
app.setSticky('nesw')
app.setStretch('both')
app.addListBox('listOfFiles')
app.setListBoxChangeFunction('listOfFiles', listCallback)

# Umbestäpselt sama mis tkinteris oli .mainloop()
# Ehk loodetavasti on kõik valmis, ja võib käivitada.
app.go()