import os
from forms import  AddRecipeForm , DelForm
from flask import Flask, render_template, url_for, redirect
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
app = Flask(__name__)
# Key for Forms
app.config['SECRET_KEY'] = 'mysecretkey'

############################################

        # SQL DATABASE AND MODELS

##########################################
basedir = os.path.abspath(os.path.dirname(__file__))
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'data.sqlite')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)
Migrate(app,db)

class Recipe(db.Model):

    __tablename__ = 'recipes'
    id = db.Column(db.Integer,primary_key = True)
    name = db.Column(db.Text)
    content = db.Column(db.Text)


    def __init__(self,name,content):
        self.name = name
        self.content = content

    def __repr__(self):
        return f"Recipe name: {self.name}. \n Ingredients and preparation: {self.content} \n ID is {self.id}"

############################################

        # VIEWS WITH FORMS

##########################################
@app.route('/')
def index():
    return render_template("index.html")

@app.route('/add.html', methods=['GET', 'POST'])
def add():
    form = AddRecipeForm()

    if form.validate_on_submit():
        name = form.recipe_name.data
        content = form.recipe_content.data

        # Add new Puppy to database
        new_recipe = Recipe(name,content)
        db.session.add(new_recipe)
        db.session.commit()

        return redirect(url_for('recipes_list'))

    return render_template('add.html',form=form)

@app.route('/recipes_list.html', methods=['GET', 'POST'])
def recipes_list():
    recipes = Recipe.query.all()
    form = DelForm()

    try:
        if form.validate_on_submit():
            id = form.id.data
            recipe = Recipe.query.get(id)
            db.session.delete(recipe)
            db.session.commit()

            #return redirect(url_for('recipes_list.html'))
            return redirect(url_for('recipes_list'))
    except:
        return redirect(url_for('notfound'))

    return render_template('recipes_list.html', recipes=recipes, form=form)

@app.route('/notfound.html')
def notfound():
    return render_template("notfound.html")

if __name__ == '__main__':
    app.run()
