from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField, SubmitField, TextAreaField, TextField


class AddRecipeForm(FlaskForm):

    recipe_name = TextAreaField('Name of recipe:')
    recipe_content= TextAreaField("Ingredients and preparation")
    submit = SubmitField('Add')

class DelForm(FlaskForm):

    id = IntegerField('ID of recipe to remove:')
    submit = SubmitField('Remove recipe')
