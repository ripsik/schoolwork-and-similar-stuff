class struktuurid {
    public class
    TrükiD {
        TrükiD() {
            System.
                    out
                    .print(
                            "d"
                    );
        }

        TrükiD(
                boolean
                        suur) {
            if
                    (suur) {
                System.
                        out
                        .print(
                                "D"
                        );
            }
        }
    }

   public class
    TrükiE
            extends
            TrükiD {
        TrükiE() {
            System.
                    out
                    .print(
                            "e"
                    );
        }

        TrükiE(
                boolean
                        suur) {
            super
                    (suur);
            if
                    (suur) {
                System.
                        out
                        .print(
                                "E"
                        );
            }
        }
    }

   public static class
    TrükiF
            extends
            TrükiE {
        TrükiF(
                boolean
                        suur) {
            if
                    (suur) {
                System.
                        out
                        .print(
                                "F"
                        );
            }
        }
    }

    public static void main(String[] args) {
TrükiD D=new TrükiF(false);
    }
}