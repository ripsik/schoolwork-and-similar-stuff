import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Koeravalija { //siin tehakse nimekiri (list) tõugudest (koos omadustega), millede hulgast sobilikke otsitakse
    private List<Koer> tõud;

    public Koeravalija() throws IOException {
        this.tõud = new ArrayList<>();
        InputStream baidid = new FileInputStream("lemmikloomad.txt");
        InputStreamReader tekst = new InputStreamReader(baidid, "UTF-8");
        BufferedReader puhverdatud = new BufferedReader(tekst);

        String rida = puhverdatud.readLine();
        String[] jupid;

        while (rida != null) {
            jupid = rida.split(";");
            if (jupid[0].equals("koer")) {
                //tõud.add(new Koer(jupid[1].trim(),Koer.Suurus.valueOf(jupid[2].trim()),Koer.Iseloom.valueOf(jupid[3].trim())));}
                tõud.add(new Koer(jupid));
            }
            rida = puhverdatud.readLine(); // loeb järgmise rea. kui ei saa, tagastab nulli

        }
        puhverdatud.close();

    }

    public String suvalineSobiv(Koer.Suurus suurus, Koer.Iseloom iseloom) {
        List<String> sobivad = new ArrayList<>();
        for (Koer koer : tõud) {
            if (suurus == koer.getSuurus() && iseloom == koer.getIseloom()) {
                sobivad.add(koer.getKoeratõug());
            }
        }
        if (sobivad.size() == 0) {
            return "Kahjuks sobivat tõugu ei leitud";
        }
        Random rand = new Random(); //et valik oleks lihtsam, tagastatakse vaid üks juhuslik sobilik tõug, kuigi sobilikke võib olla mitu
        int n = rand.nextInt(sobivad.size());
        return sobivad.get(n);
    }
}