import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class Poomismäng extends JFrame implements ActionListener {
    private JButton mangija1, mangija2;
    private JPanel paneel;
    private JLabel label1, label2, label3, label4;
    private JTextField sona, taht;
    private String sona1 = "";
    private String sonasalvesta = "";
    private String sona2 = "";
    private int pakkumistearv = 0;

    public Poomismäng() {
        setLayout(new FlowLayout());
        paneel = new JPanel();
        paneel.setPreferredSize(new Dimension(490, 450));
        paneel.setBackground(Color.lightGray);

        mangija1 = new JButton("Mängija 1");
        mangija1.addActionListener(this);

        mangija2 = new JButton("Mängija 2");
        mangija2.addActionListener(this);
        mangija2.setEnabled(false);

        label1 = new JLabel("Kirjuta siia  sõna");
        label2 = new JLabel("Paku täht");
        label3 = new JLabel("Arva ära sõna ");
        label4 = new JLabel(" ");

        sona = new JTextField(25);
        taht = new JTextField(10);
        taht.setEnabled(false);

        add(label1);
        add(sona);
        add(mangija1);
        add(paneel);
        add(mangija2);
        add(label2);
        add(taht);
        add(label3);
        add(label4);

        setSize(600, 600);
        setTitle("Poomismäng");
        setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        //mängija 1 poolt kirjutatud sõna peab olema vähemalt 3 kuni 20 tähepikkust. Sõna lowercase'itakse, kuna muidu on vastasel raske
        //ära arvata
        if (e.getSource() == mangija1) {
            String uussona1 = sona.getText();
            int sonapikkus = uussona1.length();
            if (sonapikkus >= 3 && sonapikkus <= 20) {
                setSona1(uussona1.toLowerCase());
                setsonasalvesta(uussona1.toLowerCase());
                String uussona2 = "";

                for (int i = 0; i < sonapikkus; i++) {
                    uussona2 = uussona2 + "_";
                }

                setSona2(uussona2 + "  " + sonapikkus + " tähest koosneb.");
                sona.setText(" ");
                label4.setText(sona2);
                Mangija2Valjad();
                repaint();
            } else {
                sona.setText("KONTROLLIGE, kas te kirjutasite sõna.");
            }
        }
        if (e.getSource() == mangija2) {
            //poomisväli
            Graphics g = paneel.getGraphics();
            Graphics2D joonistus = (Graphics2D) g;

            joonistus.setColor(Color.BLACK);
            joonistus.setStroke(new BasicStroke(8));


            String sone = taht.getText();
            sone = sone.toLowerCase();

            if (sone.length() == 1) {
                if (sona1.contains(sone)) {
                    char c = sone.charAt(0);

                    int pikkus = sonapikkus(sona1, c);

                    for (int i = 0; i < pikkus; i++) {
                        int index = sona1.indexOf(sone);
                        setSona1(sona1.replaceFirst(sone, " "));
                        String uussona2 = sona2.substring(0, index) + sone + sona2.substring(index + 1);
                        setSona2(uussona2);
                    }

                    label4.setText(sona2);
                    taht.setText("");
                } else {
                    taht.setText("");
                    pakkumistearv++;


                    //"nii öelda animatsioon, kus ära puuakse"
                    switch (pakkumistearv) {
                        case 1:
                            joonistus.drawLine(100, 100, 100, 320);
                            break;
                        case 2:
                            joonistus.drawLine(100, 100, 300, 100);
                            break;
                        case 3:
                            joonistus.setStroke(new BasicStroke(3));
                            joonistus.drawLine(250, 100, 250,150);
                            break;
                        case 4:
                            joonistus.fillOval(235, 150, 30, 30);
                            break;
                        case 5:
                            joonistus.setStroke(new BasicStroke(4));
                            joonistus.drawLine(250,170, 250, 220);
                            break;
                        case 6:
                            joonistus.setStroke(new BasicStroke(3));
                            joonistus.drawLine(250, 220, 270, 260);
                            break;
                        case 7:
                            joonistus.setStroke(new BasicStroke(3));
                            joonistus.drawLine(250, 220, 230, 260);
                            break;
                        case 8:
                            joonistus.setStroke(new BasicStroke(3));
                            joonistus.drawLine(250, 190, 280, 230);
                            break;
                        case 9:
                            joonistus.setStroke(new BasicStroke(3));
                            joonistus.drawLine(250, 190, 220, 230);
                            break;
                    }

                }
            } else {
                taht.setText("Sisesta ainult üks täht");
            }

            // mängu lõpp, võitja välja kuulutamine
            if (pakkumistearv == 9) {
                g.drawString("MÄNGIJA 1 VÕITIS", 120, 50);
                pakkumistearv = 0;
                Mangija1Valjad();

            } else if (!sona2.contains("_")) {
                pakkumistearv = 0;
                g.drawString(" MÄNGIJA 2, SA VÕITSID", 120, 50);
                Mangija1Valjad();

            }
        }
    }

    // sõna pikkuse arvutamine
    public int sonapikkus(String sona, char taht) {
        int arv = 0;
        for (int i = 0; i < sona.length(); i++) {
            if (sona.charAt(i) == taht) {
                arv++;
            }
        }
        return arv;
    }

    //Mängija 1 väljad, mida mägnija 1 saab ainult muuta

    public void Mangija1Valjad() {
        sona.setEnabled(true);
        mangija1.setEnabled(true);
        taht.setEnabled(false);
        mangija2.setEnabled(false);

    }

    // Mängija 2 väljad, mida mängija 2 saab muuta
    public void Mangija2Valjad() {
        sona.setEnabled(false);
        mangija1.setEnabled(false);
        taht.setEnabled(true);
        mangija2.setEnabled(true);

    }

    public String getSona1() {
        return sona1;
    }

    public void setSona1(String sona1) {
        this.sona1 = sona1;
    }

    public String getsona2() {
        return sona2;
    }

    public void setSona2(String sona2) {
        this.sona2 = sona2;
    }

    public String getsonasalvesta() {
        return sonasalvesta;
    }

    public void setsonasalvesta(String sonasalvesta) {
        this.sonasalvesta = sonasalvesta;
    }

    public static void main(String[] args) {
        JFrame Hangman = new Poomismäng();
    }
}