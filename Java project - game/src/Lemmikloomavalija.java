import javax.swing.*;
import java.io.IOException;
import java.util.List;

public class Lemmikloomavalija {
    public static final String[] lemmikloom = {"koer", "kass", "kala" , "ei taha lemmiklooma"}; //valikvastused, mille hulgast kasutaja peab valima, ei teki olukorda, et tekst valesti sisestatakse

    public static final String[] koerasuurused = {"väike", "keskmine", "suur"}; //valikvastused
    public static final String[] koeraIseloomud = {"valvekoer", "sõbralik", "rahulik", "aktiivne", "intelligentne"};//valikvastused

    public static final String[] kassiKarvastikud = {"karvutu", "lühikarvaline", "pikakarvaline"};//valikvastused
    public static final String[] kassiIseloomud = {"sülekass", "rahulik", "elavaloomuline"};//valikvastused

    public static final String[] kalaPidamiseKeerukused = {"algajatele", "ekspertidele"};//valikvastused
    public static final String[] kalaSuurused = {"väike", "keskmine", "suur"};//valikvastused
    public static final String[] kalaVälimused = {"tagasihoidlik", "värvikirev"};//valikvastused


    public static void main(String[] args) throws IOException {
        JFrame frame = new JFrame("OOP 1. rühmatöö"); //kasutajaga suhtlemiseks väike aknake, koodijupp pärit juhendist, neti abil natuke muudetud (valikvastustega)

        String valitudLemmik = "";
        if (args != null)
            valitudLemmik = args[0];
        if (valitudLemmik.isEmpty()){
            valitudLemmik = (String) JOptionPane.showInputDialog(frame,
                "Vali, millist lemmiklooma sooviksid: ",
                "Eelistatuim lemmikloom",
                JOptionPane.QUESTION_MESSAGE,
                null,
                lemmikloom,
                lemmikloom[0]);}
        if (valitudLemmik == "koer") {
            String koerasuurus = (String) JOptionPane.showInputDialog(frame,
                    "Vali, kui suurt koera sooviksid: ",
                    "Eelistatuim lemmikloom",
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    koerasuurused,
                    koerasuurused[0]);

            String koeraIseloom = (String) JOptionPane.showInputDialog(frame,
                    "Vali, millise iseloomuga koera sooviksid: ",
                    "Eelistatuim lemmikloom",
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    koeraIseloomud,
                    koeraIseloomud[0]);
            Koeravalija koeravalija = new Koeravalija();
            String parimsuvalineKoer = koeravalija.suvalineSobiv(Koer.Suurus.valueOf(koerasuurus), Koer.Iseloom.valueOf(koeraIseloom));
            //System.out.println(parimsuvaline);
            JOptionPane.showMessageDialog(frame, "Sulle sobivaim koeratõug: " + parimsuvalineKoer);
        }
        if (valitudLemmik == "kass") { //kassi puhul antakse sobivate kassitõugude nimekiri, kuna sobivaid kombinatsioone on mitu ja Kassivalija pole hetkel põhjalik
            String kassiKarvastik = (String) JOptionPane.showInputDialog(frame,
                    "Vali, millise karvapikkusega kassi sooviksid: ",
                    "Eelistatuim lemmikloom",
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    kassiKarvastikud,
                    kassiKarvastikud[0]);

            String kassiIseloom = (String) JOptionPane.showInputDialog(frame,
                    "Vali, millise iseloomuga kassi sooviksid: ",
                    "Eelistatuim lemmikloom",
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    kassiIseloomud,
                    kassiIseloomud[0]);
            Kassivalija kassivalija = new Kassivalija();
            List<String> parimsuvalineKass = kassivalija.suvalineSobiv(Kass.Karvastik.valueOf(kassiKarvastik), Kass.Iseloom.valueOf(kassiIseloom)); //tüüp teha String'iks
            // kui ühe suvalise sobiva tõu peab andma
            //System.out.println(parimsuvaline);
            if (parimsuvalineKass.size() == 0) {
                JOptionPane.showMessageDialog(frame, "Kahjuks sobivat tõugu ei leitud");
            } else {
                String nimekiri = String.join(", ", parimsuvalineKass); //teeb listist stringi, kus elementide vahel koma...
                JOptionPane.showMessageDialog(frame, "Sulle sobivaimad kassitõud: " + nimekiri);
            }
        }
        if (valitudLemmik == "kala") {//kala puhul antakse sobivate liikide nimekiri, kuna sobivaid kombinatsioone on mitu ja ilmselt tahetaksegi mitut liiki
            String kalaPidamiseKeerukus = (String) JOptionPane.showInputDialog(frame,
                    "Vali, millise pidamiskeerukusega kalaliiki sooviksid: ",
                    "Eelistatuimad kalaliigid",
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    kalaPidamiseKeerukused,
                    kalaPidamiseKeerukused[0]);

            String kalaSuurus = (String) JOptionPane.showInputDialog(frame,
                    "Vali, kui suuri kalu sooviksid: ",
                    "Eelistatuimad kalaliigid",
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    kalaSuurused,
                    kalaSuurused[0]);

            String kalaVälimus = (String) JOptionPane.showInputDialog(frame,
                    "Vali, kui suuri kalu sooviksid: ",
                    "Eelistatuimad kalaliigid",
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    kalaVälimused,
                    kalaVälimused[0]);

            Kalavalija kalavalija = new Kalavalija();
            List<String> parimadKalaLiigid = kalavalija.sobivadLiigid(Kala.PidamiseKeerukus.valueOf(kalaPidamiseKeerukus), Kala.Suurus.valueOf(kalaSuurus), Kala.Ilu.valueOf(kalaVälimus));

            //System.out.println(parimsuvaline);
            if (parimadKalaLiigid.size() == 0) {
                JOptionPane.showMessageDialog(frame, "Kahjuks sobivat liiki ei leitud");
            } else {
                String nimekiri = String.join(", ", parimadKalaLiigid); //teeb listist stringi, kus elementide vahel koma...
                JOptionPane.showMessageDialog(frame, "Sulle sobivaimad kalaliigid: " + nimekiri);
            }
        }
        if (valitudLemmik=="ei taha lemmiklooma"){
            JOptionPane.showMessageDialog(frame, "Selge, poome üles (vt konsooli).");
            Poomismäng.main(args); // lihtsalt käivitame Poomismängu


        }
        System.exit(0); //NB! vajalik programmi lõpetamiseks
    }
}

