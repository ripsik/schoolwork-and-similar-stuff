public class AdminniSisestusErind extends RuntimeException {
    public AdminniSisestusErind(String message) {
        super(message);
    }
}
