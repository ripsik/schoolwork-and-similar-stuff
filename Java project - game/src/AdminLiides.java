import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class AdminLiides {

    private static ListView<String> failisisu = new ListView<>();
    private static TextField lisalemmikText = new TextField();

    public static void start(Stage primaryStage) throws IOException {
        VBox juur = new VBox();// juur
        GridPane gridPane = new GridPane();

        Button lisaLemmikButt = new Button("Lisa");
        lisaLemmikButt.setOnAction(event -> {
            try {
                lisaUusLemmik();
                annaLemmikutefailiSisu();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        annaLemmikutefailiSisu();

        gridPane.add(new Text("Admin - tegevused omal vastutusel!"), 0, 0);
        gridPane.add(failisisu, 0, 1);
        gridPane.add(lisalemmikText, 0, 2);

        gridPane.add(lisaLemmikButt, 0, 3);

        juur.getChildren().addAll(gridPane);

        Scene stseen1 = new Scene(juur, 600, 175);
        primaryStage.setTitle("Admin");
        primaryStage.setResizable(true);
        primaryStage.setScene(stseen1);
        primaryStage.show();

    }

    /* kontrolli lisalemmikText formaadi sobivust, ja kui on ok, siis kirjutab faili juurde
     *  Formaat:
     *  koer tõug suurus iseloom
     *  kass tõug karvastik iseloom
     *  kala liik keerukus suurus ilu
     * */
    private static void lisaUusLemmik() throws IOException {
        try {
            lisalemmikText.setStyle("-fx-text-inner-color: green;");
            String sisend[] = lisalemmikText.getText().split(";");
            switch (sisend[0].trim().toUpperCase()) { //TODO: ülemklass "Lemmik", mida vastavad asjad extendivad
                case "KOER":
                    Koer koer = new Koer(sisend);
                    break;
                case "KASS":
                    Kass kass = new Kass(sisend);
                    break;
                case "KALA":
                    Kala kala = new Kala(sisend);
                    break;
                default:
                    throw new AdminniSisestusErind("Ei saa sisestatud lemmikloomast aru...");
            }


        } catch (RuntimeException e) {
            lisalemmikText.setStyle("-fx-text-inner-color: red;");
            return;
        }

        //kirjutab faili:
        Files.write(Paths.get("lemmikloomad.txt"), (lisalemmikText.getText() + "\n").getBytes(), StandardOpenOption.APPEND); //kas kirjutab? või on vaja sulgeda?


    }

    private static void annaLemmikutefailiSisu() throws IOException {

        InputStream baidid = new FileInputStream("lemmikloomad.txt");
        InputStreamReader tekst = new InputStreamReader(baidid, "UTF-8");
        BufferedReader puhverdatud = new BufferedReader(tekst);

        ObservableList<String> read = FXCollections.observableArrayList();

        String rida = puhverdatud.readLine();
        while (rida != null) {
            System.out.println("Lemmikloom: " + rida);
            read.add(rida);
            rida = puhverdatud.readLine(); // loeb järgmise rea. kui ei saa, tagastab nulli
        }
        puhverdatud.close();
        failisisu.setItems(read);
    }


}
