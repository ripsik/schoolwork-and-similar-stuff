import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Koer {
    static enum Suurus {väike, keskmine, suur} //enum tüüpi muutujal saab olla ainult need väärtused, mis siin sees näidatud

    static enum Iseloom {valvekoer, sõbralik, rahulik, aktiivne, intelligentne}

    //siin võiks olla veel paar valikukriteeriumit, mille alusel tõugu valida. Näiteks: karvaajamine, õpetatavus, hoolduse keerukus, terviseriskid, liikumisvajadus

    private String koeratõug;
    private Suurus suurus;
    private Iseloom iseloom;

    public Koer(String koeratõug, Suurus suurus, Iseloom iseloom) {
        this.koeratõug = koeratõug;
        this.suurus = suurus;
        this.iseloom = iseloom;
    }

    public Koer(String[] sisend) {
        if (sisend.length != 4) throw new AdminniSisestusErind("Vale arv elemente sisendis");

        //if (sisend[1])
        //if (Iseloom2.contains( sisend[2].trim().toLowerCase() )
        this.koeratõug=sisend[1].trim();
        try {
            this.suurus = Koer.Suurus.valueOf(sisend[2].trim().toLowerCase());
        } catch (IllegalArgumentException e) {
            throw new AdminniSisestusErind("Sobimatu suurus. Valik on: väike, keskmine, suur.");
        }
        try {
            this.iseloom = Koer.Iseloom.valueOf(sisend[3].trim().toLowerCase());
        } catch (IllegalArgumentException e) {
            throw new AdminniSisestusErind("Sobimatu iseloom. Valik on: valvekoer, sõbralik, rahulik, aktiivne, intelligentne");
        }
    }

    public String getKoeratõug() {
        return koeratõug;
    }

    public Suurus getSuurus() {
        return suurus;
    }

    public Iseloom getIseloom() {
        return iseloom;
    }

    public void setKoeratõug(String koeratõug) { // tegelikult ei ole seda vaja, aga juhendis taheti, et kuskil oleks...
        this.koeratõug = koeratõug;
    }

    @Override
    public String toString() { // seda polegi ehk vaja, praegu
        return "Koer{" +
                "koeratõug='" + koeratõug + '\'' +
                ", suurus=" + suurus +
                ", iseloom=" + iseloom +
                '}';
    }
}
