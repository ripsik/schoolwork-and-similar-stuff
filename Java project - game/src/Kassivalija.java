import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Kassivalija { //siin tehakse nimekiri (list) tõugudest (koos omadustega), millede hulgast sobilikke otsitakse
    private List<Kass> tõud;

    public Kassivalija() throws IOException {
        this.tõud= new ArrayList<>();

        InputStream baidid = new FileInputStream("lemmikloomad.txt");
        InputStreamReader tekst = new InputStreamReader(baidid, "UTF-8");
        BufferedReader puhverdatud = new BufferedReader(tekst);

        String rida = puhverdatud.readLine();
        String[] jupid;

        while (rida != null) {
            jupid = rida.split(";");
            if (jupid[0].equals("kass")){
                try {
                    tõud.add(new Kass(jupid /*[1],Kass.Karvastik.valueOf(jupid[2]),Kass.Iseloom.valueOf(jupid[3])*/ ));  //failireast konstruktor on ju olemas
                } catch (AdminniSisestusErind e) {
                    System.out.println("Ignoreerin: " + e); //fail peaks olema veatu, aga ühel juhul ei olnud...
                }
            }
            rida = puhverdatud.readLine(); // loeb järgmise rea. kui ei saa, tagastab nulli
        }
        puhverdatud.close();

    }
    public List<String> suvalineSobiv(Kass.Karvastik suurus, Kass.Iseloom iseloom){
        List<String> sobivad= new ArrayList<>();
        for (Kass kass : tõud){
            if (suurus==kass.getKarvastik() && iseloom==kass.getIseloom()){
                sobivad.add(kass.getKassitõug());
            }
        }
        //if (sobivad.size()==0){                       //kui vaja tagastada vaid üks (suvaline sobiv) tõug, siis kasutada seda osa ja siis suvalineSobiv on String tüüpi
        //    return "Kahjuks sobivat tõugu ei leitud";}

        //Random rand=new Random();
        //int  n = rand.nextInt(sobivad.size());
        //return sobivad.get(n);
        return sobivad;
    }
}