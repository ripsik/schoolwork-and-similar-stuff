
import java.io.*;
import java.util.ArrayList;
import java.util.List;

    public class Kalavalija{ //siin tehakse nimekiri (list) liikidest (koos omadustega), millede hulgast sobilikke otsitakse
        private List<Kala> liigid;

        public Kalavalija() throws IOException {
            this.liigid = new ArrayList<>();
            InputStream baidid = new FileInputStream("lemmikloomad.txt");
            InputStreamReader tekst = new InputStreamReader(baidid, "UTF-8");
            BufferedReader puhverdatud = new BufferedReader(tekst);

            String rida = puhverdatud.readLine();
            String[] jupid;

            while (rida != null) {
                jupid = rida.split(";");
                if (jupid[0].equals("kala")){
                    //liigid.add(new Kala(jupid[1],Kala.PidamiseKeerukus.valueOf(jupid[2]),Kala.Suurus.valueOf(jupid[3]),Kala.Ilu.valueOf(jupid[4])));}
                    liigid.add(new Kala (jupid));
                }
                rida = puhverdatud.readLine(); // loeb järgmise rea. kui ei saa, tagastab nulli
            }
            puhverdatud.close();

        }

        public List<String> sobivadLiigid(Kala.PidamiseKeerukus keerukus, Kala.Suurus suurus, Kala.Ilu ilu) {
            List<String> sobivad = new ArrayList<>();
            for (Kala kala : liigid) {
                if (keerukus == kala.getPidamiseKeerukus() && suurus == kala.getSuurus() && ilu == kala.getIlu()) {
                    sobivad.add(kala.getKalaliik());
                }

            }
            return sobivad;

        }
    }
