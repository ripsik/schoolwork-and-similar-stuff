public class Kass {
    static enum Karvastik {karvutu, lühikarvaline, pikakarvaline}

    static enum Iseloom {sülekass, rahulik, elavaloomuline}

    //siin võiks olla veel paar valikukriteeriumit, mille alusel tõugu valida. Näiteks: karvaajamine, hoolduse keerukus, sobivus laste ja loomadega

    private String kassitõug;
    private Karvastik suurus;
    private Iseloom iseloom;

    public Kass(String kassitõug, Karvastik suurus, Iseloom iseloom) {
        this.kassitõug = kassitõug;
        this.suurus = suurus;
        this.iseloom = iseloom;
    }
    public Kass(String[] sisend) {
        if (sisend.length != 4) throw new AdminniSisestusErind("Vale arv elemente sisendis");

        //if (sisend[1])
        //if (Iseloom2.contains( sisend[2].trim().toLowerCase() )
        this.kassitõug=sisend[1].trim();
        try {
            this.suurus = Kass.Karvastik.valueOf(sisend[2].trim().toLowerCase());
        } catch (IllegalArgumentException e) {
            throw new AdminniSisestusErind("Sobimatu karvastik. Valik on: karvutu, lühikarvaline, pikakarvaline.");
        }
        try {
            this.iseloom = Kass.Iseloom.valueOf(sisend[3].trim().toLowerCase());
        } catch (IllegalArgumentException e) {
            throw new AdminniSisestusErind("Sobimatu iseloom. Valik on: sülekass, rahulik, elavaloomuline");
        }
    }
    public String getKassitõug() {
        return kassitõug;
    }

    public Karvastik getKarvastik () {
        return suurus;
    }

    public Iseloom getIseloom() {
        return iseloom;
    }

    @Override //seda polegi vist hetkel vaja
    public String toString() {
        return "Kass{" +
                "kassitõug='" + kassitõug + '\'' +
                ", suurus=" + suurus +
                ", iseloom=" + iseloom +
                '}';
    }
}
