public class Kala {
    static enum PidamiseKeerukus {algajatele, ekspertidele}

    static enum Suurus {väike, keskmine, suur}

    static enum Ilu {tagasihoidlik, värvikirev}

    //siin võiks olla veel paar valikukriteeriumit, mille alusel tõugu valida. Näiteks: sobivus teiste kaladega, paljundamise keerukus

    private String kalaliik;
    private PidamiseKeerukus pidamiseKeerukus;
    private Suurus suurus;
    private Ilu ilu;

    public Kala(String kalaliik, PidamiseKeerukus pidamiseKeerukus, Suurus suurus, Ilu ilu) {
        this.kalaliik = kalaliik;
        this.pidamiseKeerukus = pidamiseKeerukus;
        this.suurus = suurus;
        this.ilu = ilu;
    }

    public Kala(String[] sisend) {
        if (sisend.length != 5) throw new AdminniSisestusErind("Vale arv elemente sisendis");

        //if (sisend[1])
        //if (Iseloom2.contains( sisend[2].trim().toLowerCase() )
        this.kalaliik=sisend[1].trim();
        try {
            this.pidamiseKeerukus = Kala.PidamiseKeerukus.valueOf(sisend[2].trim().toLowerCase());
        } catch (IllegalArgumentException e) {
            throw new AdminniSisestusErind("Sobimatu pidamise keerukus. Valik on: algajatele, ekspertidele.");
        }
        try {
            this.suurus = Kala.Suurus.valueOf(sisend[3].trim().toLowerCase());
        } catch (IllegalArgumentException e) {
            throw new AdminniSisestusErind("Sobimatu suurus. Valik on: väike, keskmine, suur");
        }
        try {
            this.ilu = Kala.Ilu.valueOf(sisend[4].trim().toLowerCase());
        } catch (IllegalArgumentException e) {
            throw new AdminniSisestusErind("Sobimatu värvikirevus. Valik on: tagasihoidlik, värvikirev");
        }
    }

    public String getKalaliik() {
        return kalaliik;
    }

    public PidamiseKeerukus getPidamiseKeerukus() {
        return pidamiseKeerukus;
    }

    public Suurus getSuurus() {
        return suurus;
    }

    public Ilu getIlu() {
        return ilu;
    }

    @Override
    public String toString() { //seda polegi vist hetkel vaja
        return "Kala{" +
                "kalaliik='" + kalaliik + '\'' +
                ", pidamiseKeerukus=" + pidamiseKeerukus +
                ", suurus=" + suurus +
                ", ilu=" + ilu +
                '}';
    }
}
