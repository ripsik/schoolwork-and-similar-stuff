import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;


import javax.swing.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

public class Kasutajaliides extends Application {

    Stage aken;
    Scene vaade1;
    Scene vaade2;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        aken = primaryStage;
        VBox juur = new VBox();// juur

        Text teade = new Text("Oled käivitanud programmi, mis aitab Sulle sobivaimat lemmiklooma valida. \r\n Palun tee järgnevate küsimuste juures sobilikud valikud ja vajuta Edasi");

        Text küsimus1 = new Text("Vali oma vanus");

        final ComboBox vastusVanus = new ComboBox(); //tühi combobox
        vastusVanus.getItems().addAll(
                "<10 a",
                "10-18",
                "19-60",
                "60+"
        );
        Text küsimus2 = new Text("Kas elad korteris või eramajas, aiaga või aiata?");

        final ComboBox vastusKodu = new ComboBox(); //tühi combobox
        vastusKodu.getItems().addAll(
                "väike korter",
                "suur korter",
                "eramaja väikese aiaga",
                "eramaja suure aiaga"
        );
        Text küsimus3 = new Text("Kui palju on Sul igapäevaselt võimalik lemmikloomaga tegeleda?");

        final ComboBox vastusAegLemmikule = new ComboBox(); //tühi combobox
        vastusAegLemmikule.getItems().addAll(
                "üldse mitte",
                "kuni 0,5 tundi",
                "0,5-1 tundi",
                "1-2 tundi",
                "üle 2 tunni"
        );
        Text küsimus4 = new Text("Vali, millist lemmiklooma sooviksid: ");

        final ComboBox vastusMillineLemmik = new ComboBox(); //tühi combobox
        vastusMillineLemmik.getItems().addAll(
                "koer",
                "kass",
                "kala",
                "ei taha lemmiklooma"

        );
        Button nuppEdasi = new Button("Edasi");
        nuppEdasi.setOnAction(event -> {
            try {
                vahetaVaatele2(vastusVanus, vastusKodu, vastusAegLemmikule, vastusMillineLemmik);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        Button nuppAdmin = new Button("Admin");
        nuppAdmin.setOnMousePressed(event -> {
            try {
                handleOptionsAdmin();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }); //kui hiirega Admin nupule vajutatakse, siis käivitub meetod handleOptionsAdmin

        GridPane gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 10, 10, 10));
        gridPane.setVgap(5);
        gridPane.setHgap(5);
        gridPane.setAlignment(Pos.CENTER);  //ei joonda midagi keskele?
        juur.setVgrow(gridPane, Priority.ALWAYS); //ja äkitselt hakkab ankeet akna keskele jääma...


        /*valib sunniviisiliselt midagi ära*/
        vastusVanus.getSelectionModel().selectFirst();
        vastusKodu.getSelectionModel().selectFirst();
        vastusAegLemmikule.getSelectionModel().selectFirst();
        vastusMillineLemmik.getSelectionModel().selectFirst();

        /* paigutab elemendid gridpane' lahtritesse */
        gridPane.add(teade, 0, 0, 2, 2);
        gridPane.add(küsimus1, 0, 4);
        gridPane.add(vastusVanus, 1, 4);
        gridPane.add(küsimus2, 0, 5);
        gridPane.add(vastusKodu, 1, 5);
        gridPane.add(küsimus3, 0, 6);
        gridPane.add(vastusAegLemmikule, 1, 6);
        gridPane.add(küsimus4, 0, 7);
        gridPane.add(vastusMillineLemmik, 1, 7);
        gridPane.add(nuppEdasi, 1, 9);
        gridPane.add(nuppAdmin, 1, 10);

        /* küsimused olgu paremjoondatud*/
        ColumnConstraints veerg0_seaded = new ColumnConstraints();
        veerg0_seaded.setHalignment(HPos.RIGHT);
        gridPane.getColumnConstraints().addAll(veerg0_seaded);

        /* teeb comboboxid ühelaiuseks (veeru-constraintidega ei õnnestunud) */
        vastusVanus.setPrefWidth(200);
        vastusAegLemmikule.setPrefWidth(200);
        vastusMillineLemmik.setPrefWidth(200);
        vastusKodu.setPrefWidth(200);


        juur.getChildren().addAll(gridPane);

        vaade1 = new Scene(juur, 600, 275);
        aken.setTitle("Ankeet");
        aken.setResizable(true);
        aken.setScene(vaade1);
        aken.show();
    }



    private void handleOptionsAdmin() throws IOException { // avaneb uus aken, kus valikud: lisa lemmikloom (lisab faili) ja vaata faili

        AdminLiides.start(new Stage());
    }


    /* Vaade2 täpsustab valikuid loomapõhiselt, ja annab mingeid soovitusi */
    private void vahetaVaatele2(ComboBox vastusVanus, ComboBox vastusKodu, ComboBox vastusAegLemmikule, ComboBox vastusMillineLemmik) throws IOException { // avaneb uus aken vastavalt sellele mis valik tehti küsimus4 juures ("ei taha" käivitab poomismängu,
        // muud vastava lemmiklooma valija; 2. ja 3. küsimus võetakse arvesse lemmikloomade soovitamisel;
        //küsimus1 teatab, et "oled endale lemmiklooma võtmiseks veel liiga noor, mängi parem poomismängu" OK vajutades käivitub poomismäng
        GridPane grid = new GridPane();
        VBox juur = new VBox();// juur
        grid.setPadding(new Insets(10, 10, 10, 10));
        grid.setVgap(5);
        grid.setHgap(5);
        grid.setAlignment(Pos.CENTER);  //ei joonda midagi keskele?
        juur.setVgrow(grid, Priority.ALWAYS); //ja äkitselt hakkab ankeet akna keskele jääma...

        ImageView pilt = new ImageView();
        pilt.setPreserveRatio(true);
        pilt.setFitHeight(150);

        Button tagasiVaatele1 = new Button("Uut lemmikut valima");
        tagasiVaatele1.setOnAction(e -> aken.setScene(vaade1));

        Text soovitus = new Text();
        soovitus.setFill(Color.GREEN);
        soovitus.setStyle("-fx-font: 24 arial;");

        if (vastusVanus.getValue().equals("<10 a")) {

            JFrame frame = new JFrame("Liiga noor lemmikloomasoovija");
            JOptionPane.showMessageDialog(frame, "Kahjuks oled veel liiga noor endale lemmiklooma võtmiseks. \r\n Mängi parem poomismängu");

            Platform.runLater(new Runnable() { // selleks, et pärast poomismängu lõpetamist saaks veel lemmikloomavalijaga midagi teha, teeme uue lõime või midagi taolist
                @Override public void run() { //NB! sel ajal, kui poomismängu mängitakse, ei tohi hetkel Ankeedis toimetada, programm läheb siis sassi.
                    aken.setScene(vaade1);
                    Poomismäng.main(new String[0]); //peaks käivitama poomismängu ka mingis uues aknas
                }
            });



        } else if (vastusMillineLemmik.getValue().equals("ei taha lemmiklooma")) {
            JFrame frame = new JFrame("Ei taha lemmiklooma");
            JOptionPane.showMessageDialog(frame, "Selge, mängi siis poomismängu.");
            Platform.runLater(new Runnable() {
                @Override public void run() {
                    aken.setScene(vaade1);
                    Poomismäng.main(new String[0]); //peaks käivitama poomismängu ka mingis uues aknas
                }
            });
        } else if (vastusAegLemmikule.getValue().equals("üldse mitte")) {
            JFrame frame = new JFrame("Aega ei ole");
            JOptionPane.showMessageDialog(frame, "Selge, mängi siis poomismängu.");
            Platform.runLater(new Runnable() {
                @Override public void run() {
                    aken.setScene(vaade1);
                    Poomismäng.main(new String[0]); //peaks käivitama poomismängu ka mingis uues aknas
                }
            });
        }

        /* poomismängu enam ei mängi* */
        else if (vastusMillineLemmik.getValue().equals("koer")) {
            pilt.setImage(new Image(new FileInputStream("koer.png")));
            ComboBox koeraSuurus = new ComboBox();
            ComboBox koeraIseloom = new ComboBox();
            koeraSuurus.getItems().addAll("väike", "keskmine", "suur");
            koeraIseloom.getItems().addAll("valvekoer", "sõbralik", "rahulik", "aktiivne", "intelligentne");
            koeraSuurus.getSelectionModel().selectFirst();
            koeraIseloom.getSelectionModel().select(2);

            Button soovitaNupp = new Button("Soovita");
            soovitaNupp.setOnAction(e -> {
                try {
                    Koeravalija x = new Koeravalija();
                    String parimsuvalineKoer = x.suvalineSobiv(Koer.Suurus.valueOf(koeraSuurus.getValue().toString()), Koer.Iseloom.valueOf(koeraIseloom.getValue().toString()));
                    soovitus.setText(parimsuvalineKoer);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

            });

            String sissejuhatus = "Palun täpsusta millist koera sooviksid:";
            if (vastusVanus.getValue().equals("60+") || vastusAegLemmikule.getValue().equals("kuni 0,5 tundi") || vastusAegLemmikule.getValue().equals("0,5-1 tundi")) {
                sissejuhatus += "\n Soovitame mitte valida aktiivset koera.";
            }

            if (vastusKodu.getValue().equals("väike korter")) {
                sissejuhatus += "\n Soovitame valida väike koer.";
            }


            grid.add (pilt, 0, 0, 2, 1);
            grid.add(new Text(sissejuhatus), 0, 1);
            grid.add(new Text("Suurus:"), 0, 2);
            grid.add(koeraSuurus, 1, 2);
            grid.add(new Text("Iseloom:"), 0, 3);
            grid.add(koeraIseloom, 1, 3);
            grid.add(tagasiVaatele1, 0, 4);
            grid.add(soovitaNupp, 1, 4);
            grid.add(soovitus, 0, 5, 2, 1);


        } else if (vastusMillineLemmik.getValue().equals("kass")) {
            pilt.setImage(new Image(new FileInputStream("kass.png")));
            ComboBox kassiKarvastik = new ComboBox();
            ComboBox kassiIseloom = new ComboBox();
            kassiKarvastik.getItems().addAll("karvutu", "lühikarvaline", "pikakarvaline");
            kassiIseloom.getItems().addAll("sülekass", "rahulik", "elavaloomuline");
            kassiKarvastik.getSelectionModel().select(1);
            kassiIseloom.getSelectionModel().select(1);

            Button soovitaNupp = new Button("Soovita");
            soovitaNupp.setOnAction(e -> {
                try {
                    Kassivalija x = new Kassivalija();
                    List<String> parimsuvalineKass = x.suvalineSobiv(Kass.Karvastik.valueOf(kassiKarvastik.getValue().toString()), Kass.Iseloom.valueOf(kassiIseloom.getValue().toString()));
                    String parimadKassid = "";
                    for (String kass : parimsuvalineKass) {
                        parimadKassid += kass + "\n";
                    }
                    if(parimadKassid.length()==0){
                        soovitus.setText("Kahjuks ei leidunud sobivat tõugu");
                    }
                    else{
                    soovitus.setText(parimadKassid);}
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

            });
            String sissejuhatus = "Palun täpsusta millist kassi sooviksid:";
            if (vastusVanus.getValue().equals("60+") || vastusAegLemmikule.getValue().equals("kuni 0,5 tundi")) {
                sissejuhatus += "\n Soovitame valida rahulik kass või sülekass.";
            }

            grid.add(pilt, 0,0, 2, 1);
            grid.add(new Text(sissejuhatus), 0, 1);
            grid.add(new Text("Karvastik:"), 0, 2);
            grid.add(kassiKarvastik, 1, 2);
            grid.add(new Text("Iseloom:"), 0, 3);
            grid.add(kassiIseloom, 1, 3);
            grid.add(tagasiVaatele1, 0, 4);
            grid.add(soovitaNupp, 1, 4);
            grid.add(soovitus, 0, 5, 2, 1);

        } else if (vastusMillineLemmik.getValue().equals("kala")) {
            pilt.setImage(new Image(new FileInputStream("kala.png")));
            ComboBox pidamiseKeerukus = new ComboBox();
            ComboBox kalaSuurus = new ComboBox();
            ComboBox ilu = new ComboBox();
            pidamiseKeerukus.getItems().addAll("algajatele", "ekspertidele");
            kalaSuurus.getItems().addAll("väike", "keskmine", "suur");
            ilu.getItems().addAll("tagasihoidlik", "värvikirev");
            pidamiseKeerukus.getSelectionModel().selectFirst();
            kalaSuurus.getSelectionModel().selectFirst();
            ilu.getSelectionModel().select(1);

            Button soovitaNupp = new Button("Soovita");
            soovitaNupp.setOnAction(e -> {
                try {
                    Kalavalija x = new Kalavalija();
                    List<String> parimadLiigid = x.sobivadLiigid(Kala.PidamiseKeerukus.valueOf(pidamiseKeerukus.getValue().toString()), Kala.Suurus.valueOf(kalaSuurus.getValue().toString()), Kala.Ilu.valueOf(ilu.getValue().toString()));
                    String parimadKalaLiigid = "";
                    for (String kala : parimadLiigid) {
                        parimadKalaLiigid += kala + "\n";
                    }
                    if(parimadKalaLiigid.length()==0){
                        soovitus.setText("Kahjuks ei leidunud sobivat liiki");
                    }
                    else{
                        soovitus.setText(parimadKalaLiigid);}
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

            });

            String sissejuhatus = "Palun täpsusta milliseid kalu sooviksid:";
            if (vastusVanus.getValue().equals("10-18")) {
                sissejuhatus += "\n Soovitame valida algajatele sobilikke kalaliike.";
            }
            grid.add(pilt, 0,0, 2, 1);
            grid.add(new Text(sissejuhatus), 0, 1);
            grid.add(new Text("Kala pidamise keerukus:"), 0, 2);
            grid.add(pidamiseKeerukus, 1, 2);
            grid.add(new Text("Suurus:"), 0, 3);
            grid.add(kalaSuurus, 1, 3);
            grid.add(new Text("Värvikirevus:"), 0, 4);
            grid.add(ilu, 1, 4);
            grid.add(tagasiVaatele1, 0, 5);
            grid.add(soovitaNupp, 1, 5);
            grid.add(soovitus, 0, 6, 2, 1);

        }
        juur.getChildren().addAll(grid);
        vaade2 = new Scene(juur, 400, 500);
        aken.setScene(vaade2);
    }
}
